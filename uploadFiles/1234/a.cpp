#include <stdio.h>
#include <stdlib.h>

int map(int x, int y, int z){
	int result;
	result = y*z+x;
	return result;
}

double two_d_random(int n)
{

	//Fill in code below
	//You are fee to write some other functions this function
	//Allocate memory for all the integer (x, y) coordinates inside the boundary
	//Allocate just one memory block
	//Figure out how to map between (x, y) and the index of the array
	//We can treat this allocated memory block as an integer array
	//Write a function for this mapping if needed.

	//When deciding which way to go for the next step, generate a random number as follows.
	//r = rand() % 4;
	//Treat r = 0, 1, 2, 3 as moving up, right, down and left respectively.

	//The random walk should stop once the x coordinate or y coordinate reaches $-n$ or $n$. 
	//The function should return the fraction of the visited $(x, y)$ coordinates inside (not including) the square.

	//Do not forget to free the allocated memory block before the function ends.




	int *array = (int*)malloc(((n*2+1 )*( n*2+1)) * sizeof(int));

	for (int i = 0; i <n * 2 + 1; i++){
		for (int j = 0; j <n * 2 + 1; j++){
			array[map(i, j, n)] = 0;
		}
	}


	int count = 0;
	int x = (n * 2 + 1) / 2;
	int y = (n * 2 + 1) / 2;
	while (x != 0 && x != n * 2 && y != 0 && y != n * 2){
		if (array[map(x, y, n)] == 0){
			array[map(x, y, n)] = 1;
			count++;
		}
		int r = rand() % 4;
		switch (r)
		{
		case 0:
			y--;
			break;
		case 1:
			x++;
			break;
		case 2:
			y++;
			break;
		case 3:
			x--;
			break;
		}
	}
	int size = (2 * n -1)*(2 * n -1);
	double sum = (double)count / size;
	free(array);
	return sum;

}



//Do not change the code below
int main()
{
	int trials = 1000;

	srand(12345);
	for (int n = 1; n <= 256; n *= 2)
	{
		double sum = 0.;
		for (int i = 0; i < trials; i++)
		{
			double p = two_d_random(n);
			sum += p;
		}
		printf("%d %.3lf\n", n, sum / trials);
	}
	getchar();
	getchar();
	return 0;
}

