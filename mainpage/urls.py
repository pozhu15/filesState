from django.conf.urls import url

from . import views

app_name='[mainpage]'

urlpatterns = [
    url(r'^$', views.index, name= 'index'),
    url(r'^login$', views.login, name= 'login'),
    url(r'^logon$', views.logon, name= 'logon'),
    url(r'^loginback$', views.loginback, name= 'loginback'),
    url(r'^logonback$', views.logonback, name= 'logonback'),
    url(r'^uploadfile$', views.uploadfile, name= 'uploadfile'),
    url(r'^showFiles$', views.showFiles, name= 'showFiles'),
    url(r'^download(.*)', views.download, name= 'download'),
]
