# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render_to_response

import random
import os
from  django.conf import settings
from mainpage.models import modelUserMsg  

# Create your views here.

def index(request):     #设置cookies  
    response= render_to_response('index.html') 
    #if not os.path.exists(os.path.join(settings.BASE_DIR, "username.txt")):
        #frecode= open(os.path.join(settings.BASE_DIR, "username.txt"), 'w')#记录用户名
        #frecode.close()
    if "user_cookie" not in request.COOKIES:
        strlist="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
        usermess=''
        for i in range(20):
            usermess=usermess+strlist[random.randint(0,len(strlist)-1)]
        #print(usermess)
        response.set_cookie('user_cookie',usermess,365*24*60*60*1)#设置
        response.set_cookie('user_name',' ',365*24*60*60*1)#设置用户名
        response.set_cookie('user_passd',' ',365*24*60*60*1)#设置密码
    return response  
	
def login(request):
    return render_to_response('login.html')
def logon(request):
    return render_to_response('logon.html')
    
from django.views.decorators.csrf import csrf_exempt	#这两句是让下面的函数接受form表单的上传文件不会出CSRF的错误，在html 的form中加一句 {% csrf_token %}

@csrf_exempt
def logonback(request):  #注册
    uname=request.POST['username']
    pword=request.POST['password']   
    if 'logon' in request.POST:     #注册 ,logon为input中的name属性
        if len(uname)<4 or len(pword)<4 :
            context={
                'errormessage':"用户名或密码长度不能小于等于4！",
                }
            return render_to_response('logon.html',context)
   
        if modelUserMsg.objects.filter(userName=uname).count()>0:
            context={
            'errormessage':"用户名已注册！",
            }
            response= render_to_response('logon.html',context) 
            return response #注册失败
        
        #注册成功
        pro=modelUserMsg.objects.create(#存入数据库
			userName = uname,
			passWord = pword,
		)
        pro.save()
        
        context={
        'errormessage':"注册成功请在登录客户端登录！",
        }
        response= render_to_response('logon.html',context) 
        return response 
        
        response= render_to_response('login.html')
        return response
    if 'delete' in request.POST:     #注销
        if len(uname)<1 or len(pword)<1 :
            context={
                'errormessage':"请正确输入要注销的账户名和密码",
                }
            return render_to_response('logon.html',context)

        modelUserMsg.objects.filter(userName=uname).delete()#删除
        context={
        'errormessage':"操作完成",
        }
        response= render_to_response('logon.html',context) 
        return response 
    
@csrf_exempt
def loginback(request):     #登录
    uName=request.POST['username']
    pWord=request.POST['password']
    if len(uName)<1 or len(pWord)<1 :
        return render_to_response('login.html')
    umess = modelUserMsg.objects.filter(userName=uName).values_list("passWord")
    for data in umess:
        if  pWord==data[0]:
            #return HttpResponse("helloworld")
            context=dict(
                fname=getFilesName(request.POST["username"]),
                uname=uName,
                pword=pWord,
            )
            response= render_to_response('loginback.html',context) 
            response.set_cookie('user_name',uName,24*60*60)#设置用户名,1天
            response.set_cookie('user_passd',pWord,24*60*60)#设置密码
            #return HttpResponse("helloworld")
            return response #登录成功
    
    #登录失败 
    context={
    'errormessage':"用户名或密码错误请重新输入！",
    }
    response= render_to_response('login.html',context)
    return response
    
def showFiles(request):
    context=dict(
        fname=getFilesName(request.COOKIES["user_name"]),
        uname=request.COOKIES["user_name"],
        pword=request.COOKIES["user_passd"],
    )
    response= render_to_response('loginback.html',context) 
    return response
    
@csrf_exempt	
def uploadfile(request):
    if request.method == 'POST':# 获取对象
        obj = request.FILES.get('File')
        # 上传文件的文件名
        #print(obj.name)
        ucname = request.COOKIES["user_name"]
        uploaddir = os.path.join(settings.BASE_DIR, "uploadFiles")
        if  os.path.exists(os.path.join(uploaddir,ucname)):	#新建上传项目路径
            pass
        else:
            os.mkdir(os.path.join(uploaddir,ucname))

        uploaddir=os.path.join(uploaddir,ucname)		#整合之后的文件	
        #print(uploaddir+ucookie)
        fileNameb=os.path.join(uploaddir, obj.name)
        print(fileNameb)
        fupload = open(fileNameb, 'wb')
        for chunk in obj.chunks():
            fupload.write(chunk)	#保存文件
        fupload.close()
    
    context=dict(
        fname=getFilesName(ucname),
        uname=request.COOKIES["user_name"],
        pword=request.COOKIES["user_passd"],
    )
    response= render_to_response('loginback.html',context) 
   # for i in getFilesName(ucname):
        #print(i)
    return response


   
def getFilesName(uname):
    filesName=[]
    uploaddir = os.path.join(settings.BASE_DIR, "uploadFiles")
    uploaddir=os.path.join(uploaddir,uname)		
    for parent,dirs,files in os.walk(uploaddir):
        for filename in files:
            filesName.append(filename)
    return filesName


def download(request,fileName):
    file = open(os.path.join(settings.BASE_DIR, "uploadFiles",request.COOKIES["user_name"],fileName), 'rb')
    response = HttpResponse(file)
    response['Content-Type'] = 'application/octet-stream' #设置头信息，告诉浏览器这是个文件
    response['Content-Disposition'] = 'attachment;filename='+fileName
    return response

